import { proxy } from 'valtio';
import layoutState from './layout_state';
import loginState from './login_state';

const state = proxy({
    ...loginState,
    ...layoutState
});