import { useEffect } from "react";
import { FileInput, Input } from "../../components/common/Input";
import FetchService from "../../services/fetch_service";
import { uploadFile } from "../../services/upload_service";
import MainLayout from '../../layouts/main_layout';
import Sidebar from "../../components/admin/Sidebar";
import DishesManagement from "../../components/admin/DishesManagement";

const Admin = () => {
  const onChange = async (formData) => {
    uploadFile(formData)
      .then((res) => res.json())
      .then((result) => {
        console.log(result);
      });
  };

  useEffect(() => {
    FetchService.get("dishes").then((res) => {
      console.log(res.data);
    });
  }, []);

  return (
    <MainLayout showNav={false}>
      <Sidebar />
      <DishesManagement />
    </MainLayout>
  );
};

export default Admin;
