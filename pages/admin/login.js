import Button from "../../components/common/Button";
import { FancyDivider } from "../../components/common/Fancy";
import { Input } from "../../components/common/Input";

export default function AdminLogin() {
  return (
    <div className="flex">
      <div
        className="flex-1 h-screen"
        style={{
          backgroundImage: "url('/images/home_bg.jpg')",
          backgroundSize: "cover",
        }}
      ></div>
      <div className="flex-1 h-screen px-16">
        <form className="bg-white px-12 mb-4">
          <div className="flex flex-col h-screen py-8">
            <div className="flex-auto">
              <div className="text-gray-800 text-2xl flex py-2 mb-4">
                Bienvenue sur l'interface{" "}
                <span className="text-blue-600 ml-2">administrateur</span>
              </div>
              <p className="text-gray-600">
                D'ici vous pourrez gérer votre restaurant.
              </p>
              <FancyDivider />
            </div>
            <div className="flex-auto">
              <h3 className="mb-4 text-lg font-bold">
                Connectez-vous pour commencer
              </h3>
              <div className="mb-2">
                <Input
                  name="username"
                  type="text"
                  required
                  autoFocus
                  placeholder="Nom d'utilisateur"
                />
              </div>
              <div className="mb-6">
                <Input
                  type="password"
                  placeholder="Mot de passe"
                  name="password"
                  required
                  autoComplete="current-password"
                />
              </div>
              <div className="flex items-center justify-between">
                <Button text="Se connecter" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
