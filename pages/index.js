import MainLayout from "../layouts/main_layout";
import Link from "next/link";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import Button from "../components/common/Button";

export default function Home() {
  return (
    <MainLayout showNav={false} title="Resto App">
      <div className="w-screen h-screen flex justify-center items-center bg-home_bg bg-cover relative">
        <div className="w-full h-full z-10 absolute top-0 left-0 bg-black opacity-50"></div>
        <div className="flex flex-col w-9/12 items-center z-20 relative sm:w-7/12">
          <h1 className="mb-9 text-3xl text-white sm:text-5xl sm:mb-12">
            Resto App Waiter
          </h1>
          <p className="mb-9 text-lg text-white sm:text-2xl sm:mt-12">
            Livrons-mieux ensemble
          </p>
        </div>
        <div className="absolute bottom-16 z-20 sm:bottom-24">
          <Button
            round="full"
            moreClass="w-12 h-12 sm:w-16 sm:h-16"
            bg_color="primaryColor"
            text_color="white"
          >
            <Link href="/login">
              <FontAwesomeIcon className="w-6 h-6" icon={faChevronDown} />
            </Link>
          </Button>
        </div>
      </div>
    </MainLayout>
  );
}
