import React from 'react';
import Button from '../../../components/Button';
import logo from '../../../public/images/logo-univ.png';
import Input from '../../../components/Input';
import Image from 'next/image';
import Link from 'next/link';
import MainLayout from '../../../layouts/main_layout';

const Login = () => {
    return (
        <MainLayout showNav={false}>
            <div className="sm:relative sm:h-screen">
                <div className="sm:bg-login_bg sm:absolute sm:top-0 sm:left-0 sm:w-screen sm:h-screen sm:z-10"></div>
                <div className="sm:bg-black sm:opacity-30 sm:top-0 sm:left-0 sm:w-screen sm:h-screen sm:relative sm:z-20"></div>
                <form className="sm:bg-white sm:z-30 sm:shadow-md sm:p-8 mx-6 my-12 sm:w-1/2 sm:m-0 sm:absolute sm:top-1/2 sm:left-1/2 sm:transform sm:-translate-x-1/2 sm:-translate-y-1/2 lg:w-1/3">
                    <div className="flex justify-between items-center mb-8">
                        <p className="text-3xl text-primaryColor">Connexion</p>
                        <Image className="shadow-md" width={32} height={32} src={logo} alt="logo" />
                    </div>
                    <div className="flex flex-col mb-8">
                        <label className="mb-4">Identifiant</label>
                        <Input width="full" type="text" name="identifier" />
                    </div>
                    <div className="flex flex-col mb-8">
                        <label className="mb-4">Mot de passe</label>
                        <Input width="full" type="password" name="password" />
                    </div>
                    <Button moreClass="mb-4" round="md" width="full" bg_color="primaryColor" text_color="white">
                        <Link href="/waiter/start">Se connecter</Link>
                    </Button>
                    <Button width="full" bg_color="white" text_color="primaryColor">Mot de passe oublié ?</Button>
                </form>
            </div>
        </MainLayout>
    )
};

export default Login;
