import React from 'react';
import MainLayout from '../../../layouts/main_layout';
import Link from 'next/link';
import Input from '../../../components/Input';

const Tables = () => {
    return (
       <MainLayout showNav activeHome title="Tables">
           <div className="mx-5 sm:mx-8">
                <Input type="search" name="search_tables" placeholder="Rechecher une table" width="full" />
                <h1 className="mb-3 sm:mb-3 text-xl font-bold">Tables</h1>
                <div className="flex flex-col justify-between items-center sm:flex-row sm:justify-between sm:items-center sm:flex-wrap">
                    <p className="text-center w-full sm:w-56 my-3 bg-primaryColor text-white px-3 py-4 rounded-md">
                        <Link href="/plat">Table 001</Link>
                        </p>
                        <p className="text-center w-full sm:w-56 my-3 bg-primaryColor text-white px-3 py-4 rounded-md">
                            <Link href="/home">Table 002</Link>
                        </p>
                    <p className="text-center w-full sm:w-56 my-3 bg-primaryColor text-white px-3 py-4 rounded-md">
                            <Link href="/menu">Table 003</Link>
                    </p>
                    <p className="text-center w-full sm:w-56 my-3 bg-primaryColor text-white px-3 py-4 rounded-md">
                            <Link href="/">Table 004</Link>
                    </p>
                </div>
           </div>
       </MainLayout>
    )
};

export default Tables;
