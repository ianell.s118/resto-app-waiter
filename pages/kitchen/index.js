import Image from "next/image";
import MainLayout from "../../layouts/main_layout";
import Navbar from "../../components/kitchen/Navbar";
import OrderCard from "../../components/kitchen/OrderCard";
import Link from 'next/link';

const Kitchen = () => {
  return (
    <MainLayout showNav={false}>
      {/* <Navbar /> Za maita oe tsy ilaina lé appbar fa lasa mameno écran amin'ny tsisy dikany. */}
      <div className="flex flex-wrap justify-evenly items-center">
        {Array.from({ length: 10 }).map(() => (
          <OrderCard />
        ))}
      </div>
    </MainLayout>
  );
};

export default Kitchen;
