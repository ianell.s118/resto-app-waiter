const dish = require("./src/dishes/dish_controller");

export default async function handler(req, res) {
  switch (req.method) {
    case "POST":
      return dish.create(req, res);
    case "DELETE":
      return dish.delete(req, res);
    case "PUT":
      return dish.update(req, res);
    default:
      return dish.findAll(req, res);
  }
}

/*
Exemples de query sur la route /dishes:

GET localhost:4000/api/dishes 
POST localhost:4000/api/dishes => {name: "poulet", price: 10, description: "poulet au four"} ao amin'ny postman

*/
