const db = require("../app/database");
const log = require("../app/log");

const Dish = function (dish) {
  this.name = dish.name;
  this.category = dish.category;
  this.price = dish.price;
  this.image = dish.image;
  this.description = dish.description;
};

Dish.create = (newDish, result) => {
  db.query("INSERT INTO dishes SET ?", newDish, (err, res) => {
    if (err) {
      log.error("Error: ", err);
      result(err, null);
      return;
    }

    log.valid("Created Dish with id: ", { id: res.insertId, ...newDish });
    result(null, { id: res.insertId, ...newDish });
  });
};

Dish.findAll = (result) => {
  db.query("SELECT * FROM dishes", (err, res) => {
    if (err) {
      log.error("Error: ", err);
      result(err, null);
      return;
    }

    log.valid("Found dishes: ", res);
    result(null, res);
  });
};

Dish.findById = (id, result) => {
  db.query(`SELECT * FROM dishes WHERE id=${id}`, (err, res) => {
    if (err) {
      log.error("Error: ", err);
      result(err, null);
      return;
    }

    if (res.length) {
      log.valid("Found dish: ", res[0]);
      result(null, res[0]);
      return;
    }

    log.valid("Dish not found with id: ", id);
    result({ kind: "not_found" }, null);
  });
};

Dish.update = (id, dish, result) => {
  console.log(id, dish);
  db.query("UPDATE dishes SET ? WHERE id = ?", { ...dish, id }, (err, res) => {
    if (err) {
      log.error("Error: ", err);
      result(err, null);
      return;
    }

    if (res.affectedRows == 0) {
      log.valid("Dish not found with id: ", id);
      result({ kind: "not_found" }, null);
      return;
    }

    log.valid("Updated dish: ", { id: id, ...dish });
    result(null, { id: id, ...dish });
  });
};

Dish.remove = (id, result) => {
  db.query("DELETE FROM dishes WHERE id = ?", id, (err, res) => {
    if (err) {
      log.error("Error: ", err);
      result(err, null);
      return;
    }

    if (res.affectedRows == 0) {
      log.valid("Dish not found with id: ", id);
      result({ kind: "not_found" }, null);
      return;
    }

    log.valid("Deleted dish with id: ", id);
    result(null, res);
  });
};

module.exports = Dish;
