const Dish = require("./dish_model");

// Create and Save a new Dish
exports.create = (req, res) => {
  if (!req.body) {
    res.status(400).send({
      message: "There is no content",
    });
  }

  // Create a Dish
  const dish = new Dish({
    name: req.body.name,
    price: req.body.price,
    description: req.body.description,
    image: req.body.image,
    category: req.body.category,
  });

  // Save Dish in the database
  Dish.create(dish, (err, data) => {
    if (err) {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the Dish.",
      });

      return; // Don't want to use the "else" statement
    }

    res.send(data);
  });
};

// Retrieve all Dishs from the database.
exports.findAll = (req, res) => {
  Dish.findAll((err, data) => {
    if (err) {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving dishes.",
      });

      return;
    }

    res.send(data);
  });
};

// Find a single Dish with a DishId
exports.findOne = (req, res) => {};

// Update a Dish identified by the DishId in the request
exports.update = (req, res) => {
  Dish.update(req.query.id, new Dish(req.body), (err, data) => {
    if (err) {
      res.status(500).send({
        message: err.message || "Some error occurred while updating the Dish.",
      });

      return;
    }

    res.send(data);
  });
};

// Delete a Dish with the specified DishId in the request
exports.delete = (req, res) => {
  Dish.remove(req.query.id, (err, data) => {
    if (err) {
      res.status(500).send({
        message: err.message || "Some error occurred while removing the Dish.",
      });

      return;
    }

    res.send({ message: "Dish was deleted successfully!" });
  });
};
