const mysql = require("mysql");
const log = require("./log");

// Those variables are found inside next.config.js

const connection = mysql.createConnection({
  host: process.env.HOST,
  user: process.env.USERNAME,
  password: process.env.PASSWORD,
  database: process.env.DATABASE,
});

connection.connect((error) => {
  if (error) throw error;
  log.info("Successfully connected to the database");
});

module.exports = connection;
