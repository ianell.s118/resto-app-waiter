const chalk = require("chalk");

module.exports = {
  error: (text) => console.log(chalk.red(text)),
  warning: (text) => console.log(chalk.yellow(text)),
  info: (text) => console.log(chalk.blue(text)),
  text: (text) => console.log(chalk.white(text)),
  valid: (text) => console.log(chalk.green(text)),
};
