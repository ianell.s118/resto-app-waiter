const app = require("express")();
const server = require("http").Server(app);
const io = require("socket.io")(server);
const next = require("next");

const dev = process.env.NODE_ENV !== "production";
const nextApp = next({ dev });
const nextHandler = nextApp.getRequestHandler();
const port = process.env.PORT || 4000;

io.on("connect", (socket) => {
  // socket.on("event", () => {
  //   socket.emit("something", {  });
  // });
});

nextApp.prepare().then(() => {
  app.get("*", (req, res) => {
    return nextHandler(req, res);
  });

  app.post("*", (req, res) => {
    return nextHandler(req, res);
  });

  app.put("*", (req, res) => {
    return nextHandler(req, res);
  });

  app.delete("*", (req, res) => {
    return nextHandler(req, res);
  });

  server.listen(port, (err) => {
    if (err) throw err;

    console.log(`Server running on port ${port}`);
  });
});
