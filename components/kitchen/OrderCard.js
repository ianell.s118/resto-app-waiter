import React from 'react';
import OrderItem from './OrderItem';
import OrderInfos from './OrderInfos';

const OrderCard = () => {
    return (
        <div className="shadow-all w-1/5 h-1/3 m-4 rounded-md">
            <div className="bg-secondaryColor text-white px-4 py-2 flex justify-between items-center rounded-t-md">
                <p>Table 001</p>
                <p>Commande: #3456</p>
            </div>
            <OrderItem/>
            <OrderItem/>
            <OrderItem/>
            <OrderInfos />
        </div>
    )
};

export default OrderCard;
