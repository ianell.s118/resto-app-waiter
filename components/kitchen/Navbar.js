import React from 'react';
import Image from 'next/image';

const Navbar = () => {
    return (
        <div className="bg-green-800 px-4 py-3 flex justify-between items-center">
            <div className="flex justify-between w-28">
                <Image src="/images/bg_home.jpg" className="rounded-full" width="32" height="32" alt="Test" />
                <h1 className="text-lg text-white">La Villa</h1>
            </div>
            <p className="text-white text-lg">App Cuisine</p>
            <p className="w-48"></p>
        </div>
    )
}

export default Navbar;
