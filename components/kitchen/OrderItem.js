import React from 'react';
import Image from 'next/image';

const OrderItem = () => {
    return (
        <div className="flex justify-between items-center px-4 py-2">
            <Image src="/images/bg_home.jpg" width="32" height="32" className="rounded-md" alt="" />
            <p className="font-semibold">Pizza margarita</p>
            <p className="text-white bg-green-500 rounded-full px-4 py-2">2</p>
        </div>
    )
};

export default OrderItem;
