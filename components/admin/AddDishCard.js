import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

const AddDishCard = () => {
    return (
       <div className="flex flex-col justify-center items-center mb-4 w-64 h-96 border-2 border-dashed border-green-800 p-4 rounded-xl bg-green-50">
            <FontAwesomeIcon icon={faPlus} width="32" height="32" />
            <Link href="/b"><p>Ajouter un plat</p></Link>
        </div>
    )
};

export default AddDishCard;
