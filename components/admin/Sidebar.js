import { faHome, faShoppingCart, faTable, faUtensils } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Link from 'next/link';
import Image from 'next/image';

const Sidebar = () => {
    return (
        <div className="flex flex-col w-64 h-screen bg-primaryColor text-white fixed top-0 left-0">
            <div className="flex m-4">
                <Image src="/images/logo.jpg" width="28" height="24" className="rounded-full"/>
                <h1 className="text-xl pl-4">Resto App Admin</h1>
            </div>
            <Link className="" href="/kicthen">
                <div className="flex px-5 py-2">
                    <FontAwesomeIcon icon={faUtensils} width="24" height="24" />
                    <span className="ml-4">Plats</span>
                </div>
            </Link>
            <Link className="" href="/admin">
                <div className="flex px-5 py-2">
                    <FontAwesomeIcon icon={faShoppingCart} width="24" height="24" />
                    <span className="ml-4">Commandes</span>
                </div>
            </Link>
            <Link className="" href="/admin/login">
                <div className="flex px-5 py-2">
                    <FontAwesomeIcon icon={faTable} width="24" height="24" />
                    <span className="ml-4">Tables</span>
                </div>
            </Link>
        </div>
    )
}

export default Sidebar;
