import Image from 'next/image';
import Link from 'next/link';
import Button from '../common/Button';

const DishCard = () => {
    return (
        <div className="flex flex-col items-center w-64 h-96 mb-4 border-2 border-green-800 p-4 rounded-xl bg-green-50">
            <Image src="/images/bg_home.jpg" width="120" height="120" className="rounded-full" />
            <p className="font-semibold text-center break-words py-2">Pizza margarita avec sauce tomate</p>
            <p className="pb-4">Ar 30.000</p>
            <Link href="/a"><Button text="Editer" /></Link>
            <br/>
            <Link href="/b"><Button text="Supprimer" /></Link>
        </div>
    )
};

export default DishCard;
