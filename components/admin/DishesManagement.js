import MainLayout from "../../layouts/main_layout";
import AddDishCard from "./AddDishCard";
import DishCard from "./DishCard";

const DishesManagement = () => {
    return (
        <div className="ml-64 px-4">
            <h1 className="text-xl my-8">Gestion des plats</h1>
            <div className="flex justify-evenly items-center flex-wrap">
                <AddDishCard />
                <DishCard />
                <DishCard />
                <DishCard />
                <DishCard />
            </div>
        </div>
    )
};

export default DishesManagement;
