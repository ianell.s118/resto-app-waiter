import React from 'react';

const Loader = () => {
    return (
        <div className="w-full h-full flex flex-col justify-center items-center">
            <p className="text-4xl mb-3 text-primaryColor">Resto App</p>
            <div className="w-3/4 relative">
                <div className="z-10 w-full h-1 absolute top-0 left-0 bg-gray-200"></div>
                <div className="z-20 w-1/6 h-1 absolute top-0 left-0 bg-primaryColor animate-loading"></div>
            </div>
        </div>
    )
};

export default Loader;
