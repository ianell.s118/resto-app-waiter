import { useRef } from "react";

const Input = (props) => {
  return (
    <>
      <input
        {...props}
        className={`${props?.className} border-2 rounded-sm w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline`}
      />
    </>
  );
};

Input.defaultProps = {
  className: "w-full",
};

const FileInput = (props) => {
  const fileInputRef = useRef(null);
  const formRef = useRef(null);

  const onClickHandler = () => {
    fileInputRef.current?.click();
  };

  const onChangeHandler = (event) => {
    if (!event.target.files?.length) {
      return;
    }

    const formData = new FormData();

    Array.from(event.target.files).forEach((file) => {
      formData.append(event.target.name, file);
    });

    props.onChange(formData);

    formRef.current?.reset();
  };

  return (
    <form ref={formRef}>
      <button type="button" onClick={onClickHandler}>
        {props.label}
      </button>
      <input
        accept={props.acceptedFileTypes}
        multiple={props.allowMultipleFiles}
        name={props.uploadFileName}
        onChange={onChangeHandler}
        ref={fileInputRef}
        style={{ display: "none" }}
        type="file"
      />
    </form>
  );
};

FileInput.defaultProps = {
  acceptedFileTypes: "",
  allowMultipleFiles: false,
  label: "Upload",
  uploadFileName: "file",
};

export { Input, FileInput };
