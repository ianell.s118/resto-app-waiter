import { faEye } from "@fortawesome/free-regular-svg-icons";
import { faCloudUploadAlt, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useState } from "react";

const Input = ({
  width,
  height,
  type,
  name,
  value,
  placeholder,
  moreClass,
  handleChange,
  options,
}) => {
  const [isPassword, setIsPassword] = useState(true);
  const passwordType = isPassword ? type : "text";

  switch (type) {
    case "textarea":
      return (
        <textarea
          className={`w-${width} h=${height} rounded-md p-3 bg-gray-200 focus:outline-none focus:ring-2 focus:ring-primaryColor`}
          placeholder={placeholder}
          name={name}
          value={value}
          onChange={handleChange}
        ></textarea>
      );
    case "checkbox":
      return (
        <input
          className={`p-2 form-checkbox ring-2 ring-primaryColor text-primaryColor`}
          type={type}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={handleChange}
        />
      );
    case "radio":
      return (
        <input
          className={`form-radio ring-2 ring-primaryColor text-primaryColor`}
          type={type}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={handleChange}
        />
      );
    case "select":
      return (
        <select
          className={`w-${width} h-${height} block form-select focus:outline-none ring-2 ring-primaryColor`}
        >
          <option key={10000} selected disabled>
            Country
          </option>
          {options.map((option, i) => {
            return (
              <option key={i} value={option.value}>
                {option.text}
              </option>
            );
          })}
        </select>
      );
    case "password":
      return (
        <div className="flex items-center">
          <input
            className={`w-${width} h-${height} rounded-md p-3 bg-gray-200 focus:outline-none focus:ring-2 focus:ring-primaryColor`}
            type={passwordType}
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={handleChange}
          />
          <FontAwesomeIcon
            className="cursor-pointer -ml-10 w-6 h-6"
            onClick={() => setIsPassword((isPassword) => !isPassword)}
            icon={faEye}
          />
        </div>
      );
    case "file":
      return (
        <div className="cursor-pointer rounded-md px-3 py-2 flex justify-between items-center bg-primaryColor">
          <FontAwesomeIcon
            className="w-6 h-6 text-white mr-3"
            icon={faCloudUploadAlt}
          />
          <input
            className="hidden"
            type={type}
            name={name}
            onChange={handleChange}
          />
          <label className="text-white cursor-pointer">
            Choisir un fichier
          </label>
        </div>
      );
    case "search":
      return (
        <div className={`flex items-center ${moreClass}`}>
          <FontAwesomeIcon
            className="relative z-20 -mr-10 w-6 h-6"
            icon={faSearch}
          />
          <input
            className={`w-${width} h-${height} rounded-md p-3 pl-12 bg-gray-200 focus:outline-none focus:ring-2 focus:ring-primaryColor`}
            type={type}
            name={name}
            value={value}
            placeholder={placeholder}
            onChange={handleChange}
          />
        </div>
      );
    default:
      return (
        <input
          className={`w-${width} h-${height} rounded-md p-3 bg-gray-200 focus:outline-none focus:ring-2 focus:ring-primaryColor`}
          type={type}
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={handleChange}
        />
      );
  }
};

export default Input;
