const FancyDivider = () => {
  return (
    <div className="w-1/3 flex flex-row my-8">
      <div className="flex-1 h-1 bg-blue-500"></div>
      <div className="w-2"></div>
      <div className="flex-1 h-1 bg-gray-200"></div>
    </div>
  );
};

export { FancyDivider };
