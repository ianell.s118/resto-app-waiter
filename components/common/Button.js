import React from "react";

const Button = (props) => {
  return (
    <button
      className="px-4 py-2 rounded-lg text-white w-full hover:border-2 hover:border-primaryColor bg-primaryColor hover:bg-green-50 hover:text-primaryColor focus:bg-green-400"
      type={props.type}
    >
      {props.text}
    </button>
  );
};

Button.defaultProps = {
  type: "button",
};

export default Button;
