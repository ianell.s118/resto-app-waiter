module.exports = {
  env: {
    APPNAME: "Resto App",
    MONGOURI: "mongodb://localhost:27017/resto",
    HOST: "localhost",
    USERNAME: "root",
    PASSWORD: "",
    DATABASE: "restoapp",
    PORT: "1337",
  },
  reactStrictMode: true,
};
