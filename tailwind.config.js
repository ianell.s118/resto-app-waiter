module.exports = {
  purge: ["./pages/**/*.{js,ts,jsx,tsx}", "./components/**/*.{js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'className'
  theme: {
    extend: {
      colors: {
        primaryColor: "#065f46",
        secondaryColor: "#065f46",
        accentColor: "#ffffff"
      },
      customForms: (theme) => ({
        default: {
          select: {
            borderRadius: theme("borderRadius.md"),
            boxShadow: theme("boxShadow.default"),
          },
        },
      }),
      keyframes: {
        loading: {
          "0%": { transform: "translateX(0%)" },
          "100%": { transform: "translateX(500%)" },
        },
      },
      animation: {
        loading: "loading 0.9s linear infinite alternate",
      },
      backgroundImage: {
        home_bg: "url('../public/images/bg_home.jpg')",
        login_bg: "url('../public/images/login_bg.jpeg')",
      },
      boxShadow: {
        "invert-top":
          "0 -4px 3px 1px rgba(0, 0, 0, 0.1), 0 -2px 4px 1px rgba(0, 0, 0, 0.06)",
          "all": "4px 0px 3px 2px rgba(0, 0, 0, 0.1), 4px 0px 4px 1px rgba(0, 0, 0, 0.06), -4px 0px 3px 2px rgba(0, 0, 0, 0.1), -4px 0px 4px 1px rgba(0, 0, 0, 0.06),  0px 4px 3px 2px rgba(0, 0, 0, 0.1), 0px 4px 4px 1px rgba(0, 0, 0, 0.06),  0px -4px 3px 2px rgba(0, 0, 0, 0.1), 0px -4px 4px 1px rgba(0, 0, 0, 0.06)",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [require("@tailwindcss/custom-forms")],
};
