# Changelog

## Date: 18/10/2021 - Authors: Ianel TOMBOZAFY et Anelka DONGA

### Added

- Added components for admin dishes and sidebar
- Added image for dish display
- Added colors for app: primary, secondary and accent

### Changed

- Button component

### Fixed

### Removed

---

## Date: 17/10/2021 - Author: AÏLI Fida Aliotti Christino

### Added

- Added folder database_dump which contains the sql file
- Added logger
- Added database connection
- Added CRUD example

### Changed

-

### Fixed

- Input and Button references were changed (You changed path to these folder and forgot to reset paths)

### Removed

---

## Date: 14/10/2021 - Authors: Tombozafy Ianel et Donga Anelka

### Added

- Added navbar and card components for kitchen app
- Create folders inside components for the 4 apps and the common components: waiter, kitchen, cashier, admin
- Added new shadow for card components

### Changed

- Change color to green for kitchen app

### Fixed

- Images for components are working

### Removed

---

## Date: 02/10/2021 - Author: AÏLI Fida Aliotti Christino

### Added

- Socket.IO boilerplate
- Custom server for Next JS
- Added fancy divider
- Admin login page
- Kitchen page
- Menu page
- Helpers folder with a string helper
- Upload behaviour in API and upload service method

### Changed

- Rewrite "Input" component
- Rewrite "Button" component

### Fixed

### Removed
