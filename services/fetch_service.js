import axios from "axios";

class FetchService {
  constructor() {
    this.axios = axios.create({
      baseURL: "/api/",
      timeout: 10000,
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
      },
    });
  }

  async get(url) {
    return await this.axios.get(url);
  }

  async post(url, data) {
    return await this.axios.post(url, data);
  }

  async delete(url, id) {
    return await this.axios.delete(`${url}/id=${id}`);
  }

  async update(url, data) {
    return await this.axios.put(url, data);
  }
}

export default new FetchService();
