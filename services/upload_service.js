import axios from "axios";

const config = {
  headers: { "content-type": "multipart/form-data" },
  onUploadProgress: (event) => {
    console.log(
      `Current progress:`,
      Math.round((event.loaded * 100) / event.total)
    );
  },
};

const uploadFile = async (formData) => {
  return await axios.post("/api/upload", formData, config);
};

export { uploadFile };
